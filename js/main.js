/*
Створіть програму секундомір.
Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий 
Виведення лічильників у форматі ЧЧ:ММ:СС
Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції 
 */

window.onload = () => {
    const startBtn = document.querySelector("#Start-btn");
    const stopBtn = document.querySelector("#Stop-btn");
    const resetBtn = document.querySelector("#Reset-btn");
    const background = document.querySelector(".stopwatch-display");
    const timeSec= document.querySelector("#Sec");
    const timeMin= document.querySelector("#Min");
    const timeHrs= document.querySelector("#Hrs");


    let sec = 0;
    let min = 0;
    let hrs = 0;
    let t;
    let flag = false;
    
    function tick(){
        sec++;
        if (sec >= 60) {
            sec = 0;
            min++;
            if (min >= 60) {
                min = 0;
                hrs++;
            }
        }
        timeSec.textContent = (sec > 9 ? sec : "0" + sec) 
        timeMin.textContent = (min > 9 ? min : "0" + min) 
        timeHrs.textContent = (hrs > 9 ? hrs : "0" + hrs) 


    }
    startBtn.onclick = () => {
        if (!flag) {
            t = setInterval(tick, 1000);
            flag = true
        } else {
            console.error("interval вже є!")
        }
        background.classList.add("green")
        background.classList.remove("red")
        background.classList.remove("silver")
    }
    stopBtn.onclick = () => {
        clearInterval(t);
        flag = false
        background.classList.add("red")
        background.classList.remove("green")
        background.classList.remove("silver")
    }
    resetBtn.onclick = () => {
        clearInterval(t);
        flag = false
        sec = 0;
        min = 0;
        hrs = 0;
        timeSec.textContent = "00" 
        timeMin.textContent = "00" 
        timeHrs.textContent = "00" 
        background.classList.add("silver")
        background.classList.remove("red")
        background.classList.remove("green")
    }


/*    
Реалізуйте програму перевірки телефону

Використовуючи JS Створіть поле для введення телефону та кнопку збереження
Користувач повинен ввести номер телефону у форматі 000-000-00-00
Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.
*/

const phoneWrapper = document.querySelector(".phone");
const phoneForm = document.createElement("form");

const phoneInput = document.createElement("input");
const phoneBtn = document.createElement("a");

phoneWrapper.append(phoneForm)
phoneForm.prepend(phoneInput)
phoneForm.append(phoneBtn)
phoneForm.classList.add("pone-form")
phoneBtn.innerText="Зберегти"
phoneInput.classList.add("pone-input")
phoneBtn.classList.add("pone-btn")
phoneInput.placeholder="000-000-00-00"
let phoneErorr = document.createElement("div")
phoneForm.prepend(phoneErorr)
let key =  /^\d{3}\-\d{3}\-\d{2}\-\d{2}$/;

phoneBtn.onclick = () => {
    let phoneValue=phoneInput.value

    if ((key.test(phoneValue))===true){
        phoneInput.style.backgroundColor="green"

        document.location="https:risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"

    }
    else {
     
       phoneErorr.innerText=`Не вірно введено номер телефону!!!
       Введіть номер в форматі: ${phoneInput.placeholder}`
       phoneErorr.classList.add("pone-erorr")
    }
}

}